       IDENTIFICATION DIVISION.
       PROGRAM-ID. ConditionNames.
      * Using condition names (level 88) and the EVALUATE
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  Charlin           PIC X.
           88 Vowel          VALUE "a", "e", "i", "o", "u".
           88 Consonant      VALUE "b", "c", "d", "f", "g", "h"
                                   "j" THRU "n", "p" THRU "t"
                                   "v" THRU "z".
           88 Digit          VALUE "0" THRU "9".
           88 ValidCharecter VALUE "a" THRU "z", "0" THRU "9" .
       PROCEDURE DIVISION .
       Begin.
           DISPLAY "Enter lower case character . Invalid char ends."
           ACCEPT Charlin 
           PERFORM UNTIL NOT ValidCharecter 
              EVALUATE TRUE 
                 WHEN Vowel               DISPLAY "The letter " Charlin " is a vowel."
                 WHEN Consonant           DISPLAY "The letter " Charlin " is a consonant."  
                 WHEN Digit               DISPLAY Charlin " is a Digit."  
              END-EVALUATE
              ACCEPT Charlin 
           END-PERFORM
           STOP RUN.


